# Complete WebForm Exporter

- Complete WebForm Exporter makes it easy for users to export their webform
  submissions with a single click.
- It lets user to download each submission separately too, along with
  attachments and submitted information in a single zip file.
- User can quickly export multiple webform submissions with attachments in bulk
  by selecting them and using the "Export Submissions" option.

## Features

- Provide a facility to export submission of the webform result along with
  attachments information in a single zip file.
- Provide Bulk Export Webform Submissions with their attachment in zip.
- User can manage download access via "Download any webform submission managed
  files" permission.

## Requirements

1. Webform (https://www.drupal.org/project/webform)
2. PhpSpreadsheet (https://github.com/PHPOffice/PhpSpreadsheet/releases)

## Installation

### Using the Drupal User Interface (easy):

1. Navigate to the 'Extend' page (admin/modules) via the manage administrative
menu.
2. Locate the Complete WebForm Exporter module and select the checkbox next to
it.
3. Click on 'Install' to enable the Complete WebForm Exporter module.

### Or use the command line (advanced, but very efficient).

- To enable Complete WebForm Exporter module with Drush, execute the command
below: <br> `drush en complete_webform_exporter`
