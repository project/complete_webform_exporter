<?php

namespace Drupal\complete_webform_exporter\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Site\Settings;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Returns responses for Webform Each Submission Data Download routes.
 */
class CompleteWebFormExporterController extends ControllerBase {

  /**
   * The archiver manager.
   *
   * @var \Drupal\Core\Archiver\ArchiverManager
   */
  protected $archiverManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * The Stream Wrapper Manager.
   *
   * @var Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  protected $streamWrapperManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->archiverManager = $container->get('plugin.manager.archiver');
    $instance->fileSystem = $container->get('file_system');
    $instance->fileUrlGenerator = $container->get('file_url_generator');
    $instance->streamWrapperManager = $container->get('stream_wrapper_manager');
    return $instance;
  }

  /**
   * Downloads a ZIP file with the submission in an Excel and attached files.
   */
  public function downloadDownload($webform, $webform_submission) {
    $filename = $webform . '-' . $webform_submission . '-files';
    $data_filename = $webform . '-' . $webform_submission;
    $webform = Webform::load($webform);
    $webform_submission = WebformSubmission::load($webform_submission);
    $elements_managed_files = $webform->getElementsManagedFiles();
    $fids = [];
    $signature_files = [];

    foreach ($elements_managed_files as $element_name) {
      $file_ids = $webform_submission->getElementData($element_name);

      if (!is_array($file_ids) && !empty($file_ids) && is_numeric($file_ids)) {
        $file_ids = [$file_ids];
        $fids = array_merge($fids, $file_ids);
      }
    }

    // Create an Excel sheet with webform submission data.
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $elements = $webform->getElementsDecodedAndFlattened();
    // Start from the first column.
    $columnIndex = 'G';
    $sheet->setCellValue('A1', 'Serial number');
    $sheet->setCellValue('B1', 'Submission ID');
    $sheet->setCellValue('C1', 'Created');
    $sheet->setCellValue('D1', 'User');
    $sheet->setCellValue('E1', 'LANGUAGE');
    $sheet->setCellValue('F1', 'IP ADDRESS');
    $element_types = [
      'markup',
      'container',
      'fieldset',
      'webform_wizard_page',
      'processed_text',
      'webform_actions',
      'webform_flexbox',
      'hidden',
      'webform_terms_of_service',
      'webform_message',
      'value',
      'webform_variant',
    ];

    foreach ($elements as $element) {
      // Check if the element is not a container or a markup element.
      if (isset($element['#type']) && !in_array($element['#type'], $element_types)) {
        // Use the element's admin title if available, otherwise its key.
        $header = $element['#admin_title'] ?? $element['#title'] ?? $element['#key'];
        $sheet->setCellValue($columnIndex . '1', $header);
        // Move to the next column for the next header.
        $columnIndex++;
      }
    }

    if ($webform_submission) {
      $submissionData = $webform_submission->getData();
      // Assuming headers are in the first row, start data from the second row.
      $rowNumber = 2;
      $columnIndex = 'G';
      $sheet->setCellValue('A2', $webform_submission->get('serial')->value);
      $sheet->setCellValue('B2', $webform_submission->get('sid')->value);
      $sheet->setCellValue('C2', date('d-m-Y H:i:s', $webform_submission->get('created')->value));
      $sheet->setCellValue('D2', $webform_submission->get('uid')->referencedEntities()[0]->get('name')->value);
      $sheet->setCellValue('E2', $webform_submission->get('langcode')->value);
      $sheet->setCellValue('F2', $webform_submission->get('remote_addr')->value);

      $file_type_arr = [
        'managed_file',
        'webform_document_file',
        'webform_audio_file',
        'webform_image_file',
        'webform_video_file',
        'webform_signature',
      ];

      foreach ($elements as $key => $element) {
        // Check if the element is not a container or a markup element and
        // exists in submission data.
        if (
          isset($element['#type']) &&
          !in_array($element['#type'], $element_types) &&
          array_key_exists($key, $submissionData)
          ) {
          $value = '';
          if (is_array($submissionData[$key])) {
            // Convert an array to string using a comma to separate values.
            $value = implode(', ', $submissionData[$key]);
          }
          else {
            // If it's not an array, use the value as is.
            $value = $submissionData[$key];
          }

          if (in_array($element['#type'], ['radios', 'select'])) {
            $options = $element['#options'];
            $value = !empty($value) ? $options[$value] : $value;
          }

          if (in_array($element['#type'], $file_type_arr)) {
            if ($element['#type'] == 'webform_signature') {
              $uri_scheme = $element['#uri_scheme'] ?? 'public';
              $image_base_directory = $uri_scheme . '://webform/' . $webform->id();
              $image_directory = "$image_base_directory/$key/{$webform_submission->id()}";
              $image_hash = Crypt::hmacBase64('webform-signature-' . $value, Settings::getHashSalt());
              $uri = "$image_directory/signature-$image_hash.png";
              $signature_files[] = $this->streamWrapperManager->getViaUri($uri)->realpath();
            }
            else {
              $file = $this->entityTypeManager()->getStorage('file')->load($value);
              $uri = $file->getFileUri();
            }

            $value = $this->fileUrlGenerator->generateAbsoluteString($uri);
          }
          // Write submission data to the Excel sheet under the correct header.
          $sheet->setCellValue($columnIndex . $rowNumber, $value);
          // Move to the next column for the next piece of data.
          $columnIndex++;
        }
      }
      // After writing the data, reset the column index
      // if processing multiple submissions.
      // Move to the next row for the next submission (if applicable)
      $rowNumber++;
    }

    $excelFilePath = 'temporary://' . $data_filename . '.xlsx';
    $writer = new Xlsx($spreadsheet);
    $writer->save($excelFilePath);

    // Continue with zip creation.
    $filename = $filename . '.zip';
    $archive_path = $this->fileSystem->saveData('', 'temporary://' . $filename);
    $archive_options = [
      'filepath' => $archive_path,
      'flags' => \ZipArchive::OVERWRITE,
    ];
    $archive = $this->archiverManager->getInstance($archive_options);
    $zip_archive = $archive->getArchive();
    $file_entities = $this->entityTypeManager()->getStorage('file')->loadMultiple($fids);

    // Add an Excel file to zip.
    foreach ($file_entities as $file_entity) {
      $file_path = $this->fileSystem->realpath($file_entity->getFileUri());
      $zip_archive->addFile($file_path, $file_entity->getFilename());
    }

    // Add signature files to zip.
    foreach ($signature_files as $signature_file_path) {
      $zip_archive->addFile($signature_file_path, basename($signature_file_path));
    }

    $zip_archive->addFile($this->fileSystem->realpath($excelFilePath), basename($excelFilePath));
    $zip_archive->close();
    $headers = [
      'Content-Type' => 'application/force-download',
      'Content-Disposition' => 'attachment;filename="' . $filename . '"',
      'Content-Description' => ' File Transfer',
    ];
    // Ensure to delete the temporary Excel file after adding to zip.
    $this->fileSystem->delete($excelFilePath);
    return new BinaryFileResponse($archive_path, 200, $headers, TRUE);
  }

}
