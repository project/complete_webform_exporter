<?php

namespace Drupal\complete_webform_exporter\Plugin\Action;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Archiver\ArchiverManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Export the webform submissions.
 *
 * @Action(
 *   id = "webform_submission_export_action",
 *   label = @Translation("Export submissions"),
 *   type = "webform_submission"
 * )
 */
class WebformSubmissionsExporterAction extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The archiver manager.
   *
   * @var \Drupal\Core\Archiver\ArchiverManager
   */
  protected $archiverManager;

  /**
   * The entity system.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ArchiverManager $archiverManager, EntityTypeManager $entityTypeManager, FileSystem $fileSystem, FileUrlGenerator $fileUrlGenerator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $archiverManager, $entityTypeManager, $fileSystem, $fileUrlGenerator);
    $this->archiverManager = $archiverManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.archiver'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    $webform_type = reset($entities);
    // Get current date and time using Drupal's DateTime service.
    $filename = $webform_type->getWebform()->id() . '-submissions-' . date('Y-m-d-H-i-s', strtotime('now')) . '.zip';
    $archive_path = $this->fileSystem->saveData('', 'temporary://' . $filename);
    $archive_options = [
      'filepath' => $archive_path,
      'flags' => \ZipArchive::OVERWRITE,
    ];
    $archive = $this->archiverManager->getInstance($archive_options);
    $zip_archive = $archive->getArchive();

    foreach ($entities as $entity) {
      if ($entity instanceof WebformSubmissionInterface) {
        $webform_id = $entity->getWebform()->id();
        $submission_id = $entity->id();
        $webform = Webform::load($webform_id);
        $elements = $webform->getElementsDecodedAndFlattened();
        // Export attachments for each submission.
        $attachments = $this->getAttachments($webform_id, $submission_id);
        // Continue with zip creation.
        $file_entities = $this->entityTypeManager->getStorage('file')->loadMultiple($attachments);
        // Add an Excel file to zip.
        foreach ($file_entities as $file_entity) {
          $file_path = $this->fileSystem->realpath($file_entity->getFileUri());
          $zip_archive->addFile($file_path, $webform_id . '-' . $submission_id . '/' . $file_entity->getFilename());
        }
      }
    }

    // Excel sheet
    // Create a new spreadsheet.
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    // Set column headers from the first submission.
    $column = 7;
    $sheet->setCellValue('A1', 'Serial number');
    $sheet->setCellValue('B1', 'Submission ID');
    $sheet->setCellValue('C1', 'Created');
    $sheet->setCellValue('D1', 'User');
    $sheet->setCellValue('E1', 'LANGUAGE');
    $sheet->setCellValue('F1', 'IP ADDRESS');
    $element_types = [
      'markup',
      'container',
      'fieldset',
      'webform_wizard_page',
      'processed_text',
      'webform_actions',
      'webform_flexbox',
      'hidden',
      'webform_terms_of_service',
      'webform_message',
      'value',
      'webform_variant',
    ];

    foreach ($elements as $element) {
      // Check if the element is not a container or a markup element.
      if (isset($element['#type']) && !in_array($element['#type'], $element_types)) {
        // Use the element's admin title if available, otherwise its key.
        $header = $element['#admin_title'] ?? $element['#title'] ?? $element['#key'];
        $sheet->setCellValue([$column, 1], $header);
        // Move to the next column for the next header.
        $column++;
      }
    }

    // Set rows data.
    $row = 2;

    $file_type_arr = [
      'managed_file',
      'webform_document_file',
      'webform_audio_file',
      'webform_image_file',
      'webform_video_file',
      'webform_signature',
    ];

    foreach ($entities as $entity) {
      $column = 7;
      $sheet->setCellValue([1, $row], $entity->get('serial')->value);
      $sheet->setCellValue([2, $row], $entity->get('sid')->value);
      $sheet->setCellValue([3, $row], date('d-m-Y H:i:s', $entity->get('created')->value));
      $sheet->setCellValue([4, $row], $entity->get('uid')->referencedEntities()[0]->get('name')->value);
      $sheet->setCellValue([5, $row], $entity->get('langcode')->value);
      $sheet->setCellValue([6, $row], $entity->get('remote_addr')->value);
      $row_data = $entity->getData();

      foreach ($elements as $key => $element) {
        if (isset($element['#type']) && !in_array($element['#type'], $element_types)) {
          $value = '';

          if (in_array($element['#type'], $file_type_arr)) {
            if ($element['#type'] == 'webform_signature') {
              $uri_scheme = $element['#uri_scheme'] ?? 'public';
              $image_base_directory = $uri_scheme . '://webform/' . $webform->id();
              $image_directory = "$image_base_directory/$key/{$submission_id}";
              $image_hash = Crypt::hmacBase64('webform-signature-' . $value, Settings::getHashSalt());
              $uri = "$image_directory/signature-$image_hash.png";
              $value = $this->fileUrlGenerator->generateAbsoluteString($uri);
            }
            else {
              $file = $this->entityTypeManager->getStorage('file')->load($row_data[$key]);
              if ($file) {
                $uri = $file->getFileUri();
                $value = $this->fileUrlGenerator->generateAbsoluteString($uri);
              }
            }
          }
          elseif (!in_array($element['#type'], $element_types) && array_key_exists($key, $row_data)) {
            if (is_array($row_data[$key])) {
              // Convert an array to string using a comma to separate values.
              $value = implode(', ', $row_data[$key]);
            }
            else {
              // If it's not an array, use the value as is.
              $value = $row_data[$key];
            }
            if (in_array($element['#type'], ['radios', 'select'])) {
              $options = $element['#options'];
              $value = !empty($value) ? $options[$value] : $value;
            }
          }
          $sheet->setCellValue([$column, $row], $value);
          $column++;
        }
      }
      $row++;
    }

    // Write the spreadsheet to a file.
    $writer = new Xlsx($spreadsheet);
    $file_name = 'webform_submissions_' . time() . '.xlsx';
    $file_path = 'temporary://' . $file_name;
    $writer->save($file_path);
    $zip_archive->addFile($this->fileSystem->realpath($file_path), basename($file_path));
    $zip_archive->close();
    $headers = [
      'Content-Type' => 'application/zip',
      'Content-Disposition' => 'attachment;filename="' . $filename . '"',
      'Content-Description' => ' File Transfer',
    ];
    $this->fileSystem->delete($file_path);
    $response = new BinaryFileResponse($archive_path, 200, $headers, TRUE);
    $response->send();
    exit();
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // This function is required but won't be used for bulk operations.
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\webform\WebformSubmissionInterface $object */
    return $object->access('update', $account, $return_as_object);
  }

  /**
   * Helper function to retrieve attachments from a webform submission.
   *
   * @var int $webform_id
   *   The ID of the webform.
   * @var int $webform_submission_id
   *   The ID of the webform submission.
   *
   * @return \Drupal\file\FileInterface[]
   *   Array of file entities (attachments) associated with the submission.
   */
  protected function getAttachments($webform_id, $webform_submission_id) {
    $fids = [];
    // Query to load attachments from webform submission.
    $webform = Webform::load($webform_id);
    $webform_submission = WebformSubmission::load($webform_submission_id);

    if ($webform) {
      // Retrieve attachments from submission data.
      $elements_managed_files = $webform->getElementsManagedFiles();
      foreach ($elements_managed_files as $element_name) {
        $file_ids = $webform_submission->getElementData($element_name);
        if (!is_array($file_ids) && !empty($file_ids) && is_numeric($file_ids)) {
          $file_ids = [$file_ids];
          $fids = array_merge($fids, $file_ids);
        }
      }
    }

    return $fids;
  }

}
